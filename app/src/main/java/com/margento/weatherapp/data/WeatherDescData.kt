package com.margento.weatherapp.data

import com.google.gson.annotations.SerializedName

data class WeatherDescData (@SerializedName("main") var main : String,
                            @SerializedName("description") var description: String,
                            @SerializedName("icon") var icon: String)
