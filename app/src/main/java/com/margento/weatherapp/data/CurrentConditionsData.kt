package com.margento.weatherapp.data

import com.google.gson.annotations.SerializedName

data class CurrentConditionsData(@SerializedName("temp") var  temperature: Double,
                                 @SerializedName("feels_like") var feelsLike : Double,
                                 @SerializedName("temp_min") var tempMin : Double,
                                 @SerializedName("temp_max") var tempMax : Double,
                                 @SerializedName("pressure") var pressure : Double,
                                 @SerializedName("humidity") var humidity :Double)
