package com.margento.weatherapp.data

import com.google.gson.annotations.SerializedName

data class WeatherData(
    @SerializedName("id") var cityId: Int,
    @SerializedName("name") var cityName: String,
    @SerializedName("main") var conditionsData: CurrentConditionsData,
    @SerializedName("dt") var dateTime: Long,
    @SerializedName("wind") var wind: WindData,
    @SerializedName("sys") var countryData: CountryData,
    @SerializedName("weather") var weather: List<WeatherDescData>
)

