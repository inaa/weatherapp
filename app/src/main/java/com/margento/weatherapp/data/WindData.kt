package com.margento.weatherapp.data

import com.google.gson.annotations.SerializedName

data class WindData (@SerializedName("speed") var speed : Double,
                     @SerializedName("deg") var degrees : Double)