package com.margento.weatherapp.data

import com.google.gson.annotations.SerializedName

data class CitiesWeatherData (
    @SerializedName("list") var weatherData: List<WeatherData>
)