package com.margento.weatherapp.data

import com.google.gson.annotations.SerializedName

data class CountryData(@SerializedName("country") var country : String,
                        @SerializedName("sunrise") var sunrise : Double,
                        @SerializedName("sunset") var sunset : Double)