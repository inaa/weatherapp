package com.margento.weatherapp.view

enum class ErrorHandler {
    NO_API_KEY, RESPONSE_ERROR, NO_RESULT
}