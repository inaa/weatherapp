package com.margento.weatherapp.view

interface IViewHandler {
    fun showSpinner()
    fun hideSpinner()
    fun updateWeatherData(weathers: List<WeatherViewModel>)
    fun showErrorMsg(errorType: ErrorHandler)
}
