package com.margento.weatherapp.view

import com.margento.weatherapp.data.CitiesWeatherData
import com.margento.weatherapp.data.WeatherData
import com.margento.weatherapp.request.IRequestsApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class MainPresenter(val view: IViewHandler) {
    @Inject
    lateinit var api: IRequestsApi

    private fun createListForView(citiesWeatherData: CitiesWeatherData) {
        val forecasts = mutableListOf<WeatherViewModel>()
        for (weatherResponse : WeatherData in citiesWeatherData.weatherData) {
            val forecastItem = WeatherViewModel(
                cityName = weatherResponse.cityName,
                degreeDay = weatherResponse.conditionsData.feelsLike.toString(),
                date = weatherResponse.dateTime,
                icon = weatherResponse.weather[0].icon,
                description = weatherResponse.weather[0].description)
            forecasts.add(forecastItem)
        }
        view.updateWeatherData(forecasts)
    }

    fun getWeatherData(lat: Double, long: Double) {
        view.showSpinner()
        api.getWeatherForCities(lat, long).enqueue(object : Callback<CitiesWeatherData> {

            override fun onResponse(call: Call<CitiesWeatherData>, response: Response<CitiesWeatherData>) {
                response.body()?.let {
                    createListForView(it)
                    view.hideSpinner()
                } ?: view.showErrorMsg(ErrorHandler.NO_RESULT)
            }

            override fun onFailure(call: Call<CitiesWeatherData>?, t: Throwable) {
                view.showErrorMsg(ErrorHandler.RESPONSE_ERROR)
                t.printStackTrace()
            }
        })
    }
}