package com.margento.weatherapp.view

data class WeatherViewModel(
    val cityName: String,
    val degreeDay: String,
    val icon: String = "01d",
    val date: Long = System.currentTimeMillis(),
    val description: String = "No description"
)
