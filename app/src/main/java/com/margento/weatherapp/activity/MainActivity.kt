package com.margento.weatherapp.activity

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.view.Menu
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.margento.weatherapp.R
import com.margento.weatherapp.adapter.CitiesWeatherAdapter
import com.margento.weatherapp.component.DaggerIWeatherApiComponent
import com.margento.weatherapp.module.WeatherApiModule
import com.margento.weatherapp.view.ErrorHandler
import com.margento.weatherapp.view.IViewHandler
import com.margento.weatherapp.view.MainPresenter
import com.margento.weatherapp.view.WeatherViewModel
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), IViewHandler {

    private var searchIsOpen: Boolean = false
    val presenter = MainPresenter(this)
    lateinit var citiesWeatherAdapter: CitiesWeatherAdapter

    private lateinit var fusedLocationClient: FusedLocationProviderClient


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        injectDI()
        setContentView(R.layout.activity_main)
        citiesWeatherAdapter = CitiesWeatherAdapter()
        initializeForecastList()
        getWeatherForCities()
    }

    private fun initializeForecastList() {
        cityWeatherList.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = citiesWeatherAdapter
        }
    }

    private fun injectDI() {
        DaggerIWeatherApiComponent
            .builder()
            .weatherApiModule(WeatherApiModule())
            .build()
            .inject(presenter)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_menu, menu)

        val menuItem = menu?.findItem(R.id.search_button)
        val searchMenuItem = menuItem?.actionView

        if (searchMenuItem is SearchView) {
            searchMenuItem.queryHint = getString(R.string.menu_search_hint)
            searchMenuItem.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String): Boolean {
                    citiesWeatherAdapter.filter.filter(query)
                    menuItem.collapseActionView()
                    searchIsOpen = true
                    return false
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    return false
                }

            })
        }
        return true
    }

    override fun hideSpinner() {
        cityWeatherList.visibility = View.VISIBLE
        loadingSpinner.visibility = View.GONE
    }

    override fun updateWeatherData(weathers: List<WeatherViewModel>) {
        if (weathers.isEmpty()) emptyStateText.visibility = View.VISIBLE
        cityWeatherList.adapter?.safeCast<CitiesWeatherAdapter>()?.addCityWeatherData(weathers)
    }

    private fun getWeatherForCities() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
            PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            askPermission()
            return
        }
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                if (location != null) {
                    presenter.getWeatherData(location.latitude, location.longitude)
                }
            }
    }

    inline fun <reified T> Any.safeCast() = this as? T

    fun Activity.toast(toastMessage: String, duration: Int = Toast.LENGTH_SHORT) {
        Toast.makeText(this, toastMessage, duration).show()
    }

    override fun showSpinner() {
        cityWeatherList.visibility = View.GONE
        emptyStateText.visibility = View.GONE
        loadingSpinner.visibility = View.VISIBLE
    }

    override fun showErrorMsg(errorType: ErrorHandler) {
        when (errorType) {
            ErrorHandler.RESPONSE_ERROR -> toast(getString(R.string.connection_error_message))
            ErrorHandler.NO_API_KEY -> toast(getString(R.string.missing_api_key_message))
            ErrorHandler.NO_RESULT -> toast(getString(R.string.city_not_found_toast_message))
        }
        loadingSpinner.visibility = View.GONE
        emptyStateText.visibility = View.VISIBLE
    }

    override fun onBackPressed() {
        if (searchIsOpen) {
            searchIsOpen = false
            getWeatherForCities()
        }
        else
            super.onBackPressed()
    }

    fun askPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(
                this@MainActivity,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        ) {
            ActivityCompat.requestPermissions(
                this@MainActivity,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1
            )
        } else {
            ActivityCompat.requestPermissions(
                this@MainActivity,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1
            )
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            1 -> {
                if (grantResults.isNotEmpty() && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED
                ) {
                    if ((ContextCompat.checkSelfPermission(
                            this@MainActivity,
                            Manifest.permission.ACCESS_FINE_LOCATION
                        ) ===
                                PackageManager.PERMISSION_GRANTED)
                    ) {
                        Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show()
                        getWeatherForCities()
                    }
                } else {
                    Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show()
                }
                return
            }
        }
    }

}