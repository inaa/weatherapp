package com.margento.weatherapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.margento.weatherapp.R
import com.margento.weatherapp.view.WeatherViewModel
import kotlinx.android.synthetic.main.cities_weather_list_item.view.*
import java.text.SimpleDateFormat
import java.util.*

class CitiesWeatherAdapter() : RecyclerView.Adapter<CitiesWeatherAdapter.WeatherViewHolder>(), Filterable {

    var citiesWeatherList = mutableListOf<WeatherViewModel>()

    fun addCityWeatherData(list : List<WeatherViewModel>){
        citiesWeatherList.clear()
        citiesWeatherList.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.cities_weather_list_item, parent, false)
        return WeatherViewHolder(view)
    }

    override fun onBindViewHolder(holder: WeatherViewHolder, position: Int) {
        citiesWeatherList[position].let {
            holder.bind(weatherElement = it)
        }
    }

    override fun getItemCount(): Int {
        return citiesWeatherList.size
    }

    class WeatherViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(weatherElement : WeatherViewModel) {
            itemView.cNameText.text = weatherElement.cityName
            itemView.degreeText.text = "${weatherElement.degreeDay} °C ${weatherElement.description}"
            itemView.dateText.text = getDate(weatherElement.date)
            Glide.with(itemView.context)
                .load("https://openweathermap.org/img/w/${weatherElement.icon}.png")
                .into(itemView.weatherIcon)

        }

        private fun  getDate(date: Long): String {
            val timeFormatter = SimpleDateFormat("dd.MM.yyyy")
            return timeFormatter.format(Date(date*1000L))
        }

    }

    override fun getFilter(): Filter {

        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                val filterResults = FilterResults()
                if (charSearch.isEmpty()) {
                    filterResults.values = citiesWeatherList
                } else {
                    val resultList = mutableListOf<WeatherViewModel>()
                    for (row in citiesWeatherList) {

                        if (row.cityName.toLowerCase().contains(charSearch.toLowerCase(Locale.ROOT))) {
                            resultList.add(row)
                        }
                    }
                    filterResults.values = resultList
                }
                return filterResults
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                addCityWeatherData(results?.values as ArrayList<WeatherViewModel>)
            }

        }
    }

}