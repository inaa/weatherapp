package com.margento.weatherapp.module

import com.google.gson.Gson
import com.margento.weatherapp.request.IRequestsApi
import com.margento.weatherapp.request.RequestInterceptor
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module(includes = arrayOf(GsonModule::class))
class WeatherApiModule {
    @Provides
    @Singleton
    fun provideApi(gson : Gson): IRequestsApi {

        val apiClient = OkHttpClient.Builder().addInterceptor(RequestInterceptor()).build()

        return Retrofit.Builder().apply {
            baseUrl(IRequestsApi.BASE_URL)
            addConverterFactory(GsonConverterFactory.create(gson))
            client(apiClient)
        }.build().create(IRequestsApi::class.java)
    }
}