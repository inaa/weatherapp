package com.margento.weatherapp.request

import com.margento.weatherapp.data.CitiesWeatherData
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface IRequestsApi {

//    @GET("weather")
//    fun dailyForecast(@Query("q") cityName : String) : Call<WeatherData>

    @GET("find")
    fun getWeatherForCities(@Query("lat") latitude: Double, @Query("lon") longitude: Double) : Call<CitiesWeatherData>

    companion object {
        val BASE_URL = "https://api.openweathermap.org/data/2.5/"
    }


}