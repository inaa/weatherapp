package com.margento.weatherapp.component

import com.margento.weatherapp.module.WeatherApiModule
import com.margento.weatherapp.view.MainPresenter
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [WeatherApiModule::class])
interface IWeatherApiComponent {
    fun inject(presenter: MainPresenter);
}